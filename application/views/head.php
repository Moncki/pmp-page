<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>PROGRAM ME PROJECTS</title>
    <link rel="shortcut icon" href="<?php base_url() ?>plantilla/ico/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php base_url() ?>plantilla/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php base_url() ?>plantilla/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?php base_url() ?>plantilla/css/estilos.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.3/css/mdb.min.css" rel="stylesheet">
  </head>
  <body data-spy="scroll" data-target="#navegacion">
        <!-- Cabeza -->
        <header class="bg view">
          <div class="mask rgba-gradient align-items-center">
            <div class="container-fluid" id="secction1">
              <!--Navbar-->
              <nav class="navbar navbar-expand-lg navbar-expand-md navbar-dark navbar-inverse fixed-top scrolling-navbar">
                  <!-- Navbar brand -->
                  <a class="navbar-brand" href="#">Program Me Projects</a>
                  <!-- Collapse button -->
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
                    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>

                  <!-- Collapsible content -->
                  <div class="collapse navbar-collapse" id="basicExampleNav">
                    <!-- Links -->
                    <ul class="navbar-nav  ml-auto">
                      <li class="nav-item">
                        <a class="nav-link active" href="#secction1"><span class="glyphicon glyphicon-home"></span> Home
                          <span class="sr-only">(current)</span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#secction2"><span class="glyphicon glyphicon-pencil"></span> Projects</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#secction3"><span class="glyphicon glyphicon-user"></span> about us</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#secction4"><span class="glyphicon glyphicon-envelope"></span> contact us</a>
                      </li>
                    </ul>
                  </div>
                </nav>
              <!-- Landing -->
              <div class="row">
                <div class="row secction1">

                  <div class="col-md-12 col-lg-offset-3 col-md-offset-2 col-sm-offset-2" data-wow-delay="0.3s">
                    <img class="img-f" src="<?php base_url() ?>plantilla/pmp.gif" alt="">
                  </div>
                  <div class="col-md-12 text-center mt-xl-5 mb-5" data-wow-delay="0.3s">
                    <h1 class="h1-responsive co2 mt-sm-5">Bienvenido a la web donde hacemos tus sueños una realidad virtual </h1>
                    <hr class="hr-light">
                    <h6 class="mb-4 co0">Progam me projects es una organizacion creada por jovenes Venezolanos, para el desarrollo de software a las pequeñas y medianas empresas<span class="hidden-xs">  dicha organización busca el avance de la comunidad internacional al igual que vela por la excelencia y el desarrollo de las capacidades de sus miembros, reuniendo a jóvenes con pasión en el desarrollo de software y en la tecnología, contribuyendo activamente al avance de los sistemas que componen su entorno</span></h6>

                    <a class="btn purple-gradient mb-4" href="#secction2">Portfolio</a>

                    <a class="btn blue-gradient mb-4" data-toggle="modal" data-target="#modalContactForm">Contact us</a>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </header>
