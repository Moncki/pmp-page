
    <!-- Main -->
    <section class="main">
      <div class="row">
        <section class="col-md-12">

          <div class="secction fondo1" id="secction2">
            <article class="container clearfix">
              <!-- Section heading -->
              <h2 class="h1-responsive font-weight-bold my-5 text-center"><span class="glyphicon glyphicon-briefcase"></span> Our best projects</h2>

              <!-- Grid row -->
              <div class="pat row text-center">

                <!-- Grid column -->
                <div class="col-lg-4 col-md-12">
                    <!--Featured image-->
                    <div class="view overlay rounded z-depth-1">
                      <img src="https://mdbootstrap.com/img/Photos/Others/images/58.jpg" class="img-fluid" alt="Sample project image">
                      <a>
                        <div class="mask rgba-white-slight"></div>
                      </a>
                    </div>
                    <!--Excerpt-->
                    <div class="card-body pb-0">
                      <h4 class="font-weight-bold my-3">Title of the project</h4>
                      <p class="grey-text caption">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saep eveniet ut et voluptates repudiandae.
                      </p>
                      <a class="btn btn-indigo"><i class="fas fa-clone left"></i> View project</a>
                    </div>
                  </div>

                <!-- Grid column -->
                <div class="col-lg-4 col-md-6">
                    <!--Featured image-->
                    <div class="view overlay rounded z-depth-1">
                      <img src="https://mdbootstrap.com/img/Photos/Others/project4.jpg" class="img-fluid" alt="Sample project image">
                      <a>
                        <div class="mask rgba-white-slight"></div>
                      </a>
                    </div>
                    <!--Excerpt-->
                    <div class="card-body pb-0">
                      <h4 class="font-weight-bold my-3">Title of the project</h4>
                      <p class="grey-text caption">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae.
                      </p>
                      <a class="btn btn-indigo"><i class="fas fa-clone left"></i> View project</a>
                    </div>
                  </div>

                <!-- Grid column -->
                <div class="col-lg-4 col-md-6">
                    <!--Featured image-->
                    <div class="view overlay rounded z-depth-1">
                      <img src="https://mdbootstrap.com/img/Photos/Others/images/88.jpg" class="img-fluid" alt="Sample project image">
                      <a>
                        <div class="mask rgba-white-slight"></div>
                      </a>
                    </div>
                    <!--Excerpt-->
                    <div class="card-body pb-0">
                      <h4 class="font-weight-bold my-3">Title of the project</h4>
                      <p class="grey-text caption">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae.
                      </p>
                      <a class="btn btn-indigo"><i class="fas fa-clone left"></i> View project</a>
                    </div>
                  </div>
              </div>

              <!-- Grid row -->
              <div class="pat row text-center hidden-sm hidden-xs">

                    <!-- Grid column -->
                    <div class="col-lg-4 col-md-12">
                      <!--Featured image-->
                      <div class="view overlay rounded z-depth-1">
                        <img src="https://mdbootstrap.com/img/Photos/Others/images/58.jpg" class="img-fluid" alt="Sample project image">
                        <a>
                          <div class="mask rgba-white-slight"></div>
                        </a>
                      </div>
                      <!--Excerpt-->
                      <div class="card-body pb-0">
                        <h4 class="font-weight-bold my-3">Title of the project</h4>
                        <p class="grey-text caption">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae.
                        </p>
                        <a class="btn btn-indigo"><i class="fas fa-clone left"></i> View project</a>
                      </div>
                    </div>

                    <!-- Grid column -->
                    <div class="col-lg-4 col-md-6">
                      <!--Featured image-->
                      <div class="view overlay rounded z-depth-1">
                        <img src="https://mdbootstrap.com/img/Photos/Others/project4.jpg" class="img-fluid" alt="Sample project image">
                        <a>
                          <div class="mask rgba-white-slight"></div>
                        </a>
                      </div>
                      <!--Excerpt-->
                      <div class="card-body pb-0">
                        <h4 class="font-weight-bold my-3">Title of the project</h4>
                        <p class="grey-text caption">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae.
                        </p>
                        <a class="btn btn-indigo"><i class="fas fa-clone left"></i> View project</a>
                      </div>
                    </div>

                    <!-- Grid column -->
                    <div class="col-lg-4 col-md-6">
                      <!--Featured image-->
                      <div class="view overlay rounded z-depth-1">
                        <img src="https://mdbootstrap.com/img/Photos/Others/images/88.jpg" class="img-fluid" alt="Sample project image">
                        <a>
                          <div class="mask rgba-white-slight"></div>
                        </a>
                      </div>
                      <!--Excerpt-->
                      <div class="card-body pb-0">
                        <h4 class="font-weight-bold my-3">Title of the project</h4>
                        <p class="grey-text caption">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae.
                        </p>
                        <a class="btn btn-indigo"><i class="fas fa-clone left"></i> View project</a>
                      </div>
                    </div>
                  </div>
                </div>

            </article>
          </div>
          <div class="secction2 fondo2 pad" id="secction3">
            <article class="clearfix">

              <section class="team-section">
                <!-- Section heading -->
                <h2 class="h1 my-5"><span class="glyphicon glyphicon-education"></span> Specialized staff</h2>
                <!-- Grid row -->
                <div class="row text-center">

                  <!-- Grid column -->
                  <div class="col-lg-3 col-md-3 col-sm-6 pat">
                    <div class="mx-auto">
                      <img src="<?php base_url() ?>plantilla/imagenes/images.png" class="img-fluid rounded-circle z-depth-1"
                        alt="Sample avatar">
                    </div>
                    <h5 class="mt-4 mb-3 ti">Nombre Apellido</h5>
                    <p class="text-uppercase blue-text p"><strong>Graphic designer</strong></p>
                    <p class="grey-text p">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                    <ul class="list-unstyled mb-0">
                      <!-- Facebook -->
                      <a class="p-2 fa-lg fb-ic">
                        <i class="fab fa-facebook-f blue-text"> </i>
                      </a>
                      <!-- Twitter -->
                      <a class="p-2 fa-lg tw-ic">
                        <i class="fab fa-twitter blue-text"> </i>
                      </a>
                      <!-- Instagram -->
                      <a class="p-2 fa-lg ins-ic">
                        <i class="fab fa-instagram blue-text"> </i>
                      </a>
                      <!-- Github -->
                      <a class="p-2 fa-lg ins-ic">
                        <i class="fab fa-github blue-text"> </i>
                      </a>
                    </ul>
                  </div>

                  <!-- Grid column -->
                  <div class="col-lg-3 col-md-3 col-sm-6 pat">
                    <div class="mx-auto">
                      <img src="<?php base_url() ?>plantilla/imagenes/images.png" class="img-fluid rounded-circle z-depth-1"
                        alt="Sample avatar">
                    </div>
                    <h5 class="mt-4 mb-3 ti">Nombre Apellido</h5>
                    <p class="text-uppercase blue-text p"><strong>Web developer</strong></p>
                    <p class="grey-text p">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                    <ul class="list-unstyled mb-0">
                      <!-- Facebook -->
                      <a class="p-2 fa-lg fb-ic">
                        <i class="fab fa-facebook-f blue-text"> </i>
                      </a>
                      <!-- Twitter -->
                      <a class="p-2 fa-lg tw-ic">
                        <i class="fab fa-twitter blue-text"> </i>
                      </a>
                      <!-- Instagram -->
                      <a class="p-2 fa-lg ins-ic">
                        <i class="fab fa-instagram blue-text"> </i>
                      </a>
                      <!-- Github -->
                      <a class="p-2 fa-lg ins-ic">
                        <i class="fab fa-github blue-text"> </i>
                      </a>
                    </ul>
                  </div>

                  <!-- Grid column -->
                  <div class="col-lg-3 col-md-3 col-sm-6 pat">
                    <div class="mx-auto">
                      <img src="<?php base_url() ?>plantilla/imagenes/images.png" class="img-fluid rounded-circle z-depth-1"
                        alt="Sample avatar">
                    </div>
                    <h5 class="mt-4 mb-3 ti">Nombre Apellido</h5>
                    <p class="text-uppercase blue-text p"><strong>Photographer</strong></p>
                    <p class="grey-text p">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                    <ul class="list-unstyled mb-0">
                      <!-- Facebook -->
                      <a class="p-2 fa-lg fb-ic">
                        <i class="fab fa-facebook-f blue-text"> </i>
                      </a>
                      <!-- Twitter -->
                      <a class="p-2 fa-lg tw-ic">
                        <i class="fab fa-twitter blue-text"> </i>
                      </a>
                      <!-- Instagram -->
                      <a class="p-2 fa-lg ins-ic">
                        <i class="fab fa-instagram blue-text"> </i>
                      </a>
                      <!-- Github -->
                      <a class="p-2 fa-lg ins-ic">
                        <i class="fab fa-github blue-text"> </i>
                      </a>
                    </ul>
                  </div>

                  <!-- Grid column -->
                  <div class="col-lg-3 col-md-3 col-sm-6 pat">
                    <div class="mx-auto">
                      <img src="<?php base_url() ?>plantilla/imagenes/images.png" class="img-fluid rounded-circle z-depth-1"
                        alt="Sample avatar">
                    </div>
                    <h5 class="mt-4 mb-3 ti">Nombre Apellido</h5>
                    <p class="text-uppercase blue-text p"><strong>Backend developer</strong></p>
                    <p class="grey-text p">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                    <ul class="list-unstyled mb-0">
                      <!-- Facebook -->
                      <a class="p-2 fa-lg fb-ic">
                        <i class="fab fa-facebook-f blue-text"> </i>
                      </a>
                      <!-- Twitter -->
                      <a class="p-2 fa-lg tw-ic">
                        <i class="fab fa-twitter blue-text"> </i>
                      </a>
                      <!-- Instagram -->
                      <a class="p-2 fa-lg ins-ic">
                        <i class="fab fa-instagram blue-text"> </i>
                      </a>
                      <!-- Github -->
                      <a class="p-2 fa-lg ins-ic">
                        <i class="fab fa-github blue-text"> </i>
                      </a>
                    </ul>
                  </div>
                </div>
              </section>

              <section class="team-section">
                <!-- Section heading -->
                <h2 class="h1 my-5"><span class="glyphicon glyphicon-user"></span> Management set</h2>
                <!-- Grid row -->
                <div class="row text-center">

                  <!-- Grid column -->
                  <div class="col-lg-3 col-md-3 col-sm-6 pat">
                    <div class="mx-auto">
                      <img src="<?php base_url() ?>plantilla/imagenes/images.png" class="img-fluid rounded-circle z-depth-1"
                        alt="Sample avatar">
                    </div>
                    <h5 class="mt-4 mb-3 ti">Nombre Apellido</h5>
                    <p class="text-uppercase blue-text p"><strong>Graphic designer</strong></p>
                    <p class="grey-text p">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                    <ul class="list-unstyled mb-0">
                      <!-- Facebook -->
                      <a class="p-2 fa-lg fb-ic">
                        <i class="fab fa-facebook-f blue-text"> </i>
                      </a>
                      <!-- Twitter -->
                      <a class="p-2 fa-lg tw-ic">
                        <i class="fab fa-twitter blue-text"> </i>
                      </a>
                      <!-- Instagram -->
                      <a class="p-2 fa-lg ins-ic">
                        <i class="fab fa-instagram blue-text"> </i>
                      </a>
                      <!-- Github -->
                      <a class="p-2 fa-lg ins-ic">
                        <i class="fab fa-github blue-text"> </i>
                      </a>
                    </ul>
                  </div>

                  <!-- Grid column -->
                  <div class="col-lg-3 col-md-3 col-sm-6 pat">
                    <div class="mx-auto">
                      <img src="<?php base_url() ?>plantilla/imagenes/images.png" class="img-fluid rounded-circle z-depth-1"
                        alt="Sample avatar">
                    </div>
                    <h5 class="mt-4 mb-3 ti">Nombre Apellido</h5>
                    <p class="text-uppercase blue-text p"><strong>Web developer</strong></p>
                    <p class="grey-text p">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                    <ul class="list-unstyled mb-0">
                      <!-- Facebook -->
                      <a class="p-2 fa-lg fb-ic">
                        <i class="fab fa-facebook-f blue-text"> </i>
                      </a>
                      <!-- Twitter -->
                      <a class="p-2 fa-lg tw-ic">
                        <i class="fab fa-twitter blue-text"> </i>
                      </a>
                      <!-- Instagram -->
                      <a class="p-2 fa-lg ins-ic">
                        <i class="fab fa-instagram blue-text"> </i>
                      </a>
                      <!-- Github -->
                      <a class="p-2 fa-lg ins-ic">
                        <i class="fab fa-github blue-text"> </i>
                      </a>
                    </ul>
                  </div>

                  <!-- Grid column -->
                  <div class="col-lg-3 col-md-3 col-sm-6 pat">
                    <div class="mx-auto">
                      <img src="<?php base_url() ?>plantilla/imagenes/images.png" class="img-fluid rounded-circle z-depth-1"
                        alt="Sample avatar">
                    </div>
                    <h5 class="mt-4 mb-3 ti">Nombre Apellido</h5>
                    <p class="text-uppercase blue-text p"><strong>Photographer</strong></p>
                    <p class="grey-text p">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                    <ul class="list-unstyled mb-0">
                      <!-- Facebook -->
                      <a class="p-2 fa-lg fb-ic">
                        <i class="fab fa-facebook-f blue-text"> </i>
                      </a>
                      <!-- Twitter -->
                      <a class="p-2 fa-lg tw-ic">
                        <i class="fab fa-twitter blue-text"> </i>
                      </a>
                      <!-- Instagram -->
                      <a class="p-2 fa-lg ins-ic">
                        <i class="fab fa-instagram blue-text"> </i>
                      </a>
                      <!-- Github -->
                      <a class="p-2 fa-lg ins-ic">
                        <i class="fab fa-github blue-text"> </i>
                      </a>
                    </ul>
                  </div>

                  <!-- Grid column -->
                  <div class="col-lg-3 col-md-3 col-sm-6 pat">
                    <div class="mx-auto">
                      <img src="<?php base_url() ?>plantilla/imagenes/images.png" class="img-fluid rounded-circle z-depth-1"
                        alt="Sample avatar">
                    </div>
                    <h5 class="mt-4 mb-3 ti">Nombre Apellido</h5>
                    <p class="text-uppercase blue-text p"><strong>Backend developer</strong></p>
                    <p class="grey-text p">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                    <ul class="list-unstyled mb-0">
                      <!-- Facebook -->
                      <a class="p-2 fa-lg fb-ic">
                        <i class="fab fa-facebook-f blue-text"> </i>
                      </a>
                      <!-- Twitter -->
                      <a class="p-2 fa-lg tw-ic">
                        <i class="fab fa-twitter blue-text"> </i>
                      </a>
                      <!-- Instagram -->
                      <a class="p-2 fa-lg ins-ic">
                        <i class="fab fa-instagram blue-text"> </i>
                      </a>
                      <!-- Github -->
                      <a class="p-2 fa-lg ins-ic">
                        <i class="fab fa-github blue-text"> </i>
                      </a>
                    </ul>
                  </div>
                </div>
              </section>

            </article>
          </div>
          <div class="secction fondo3" id="secction4">
            <article class="clearfix">
              <div class="row">
                <div class="col-md-12 text-center">
                  <p class=" h1"><strong>CONTACT US</strong></p>
                </div>
                <div class="col-md-12 text-center">
                  <p class="font-cont"><em>We want to hear you</em></p>
                </div>
                <div class="col-lg-5 col-md-12">
                  <div class="container font-con">
                    <p>
                      <strong>Si quieres informacion sobre cualquiera de nuestros productos o estas interesado en formar parte de en nuestra empresa, rellena nuestro formulario y nos pondremos en contacto contigo lo antes posible.</strong>
                    </p>
                    <p>
                      Muchas gracias.
                    </p>
                    <p>
                      <strong>Sede central y oficinas:</strong> Puerto ordaz, villa asia por la carrera coma, La churuata, torre 9, piso 11
                    </p>
                    <p>
                      <strong>Telefono:</strong> 0286-96216963
                    </p>
                    <p>
                      <strong>Administracion:</strong> hector@gmail.com
                    </p>
                    <p>
                      <strong>Compras:</strong> eduardo@gmail.com
                    </p>
                    <p>
                      <strong>Ventas:</strong> gabriel@gmail.com
                    </p>
                    <p>
                      <strong>Calidad:</strong> andres@gmail.com
                    </p>
                    <p>
                      <strong>Comunicacion y Marketing:</strong> angel@gmail.com
                    </p>
                  </div>
                  <div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header blue-gradient text-center">
                          <h4 class="modal-title w-100 font-weight-bold">Contact us</h4>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span class="font-weight-bold" aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body mx-3">

                          <div class="md-form mb-5">
                            <i class="fas fa-user prefix grey-text"></i>
                            <input type="text" id="form34" class="form-control validate">
                            <label data-error="wrong" data-success="right" for="form34">Your name</label>
                          </div>

                          <div class="md-form mb-5">
                            <i class="fas fa-envelope prefix grey-text"></i>
                            <input type="email" id="form29" class="form-control validate">
                            <label data-error="wrong" data-success="right" for="form29">Your email</label>
                          </div>

                          <div class="md-form mb-5">
                            <i class="fas fa-tag prefix grey-text"></i>
                            <input type="text" id="form32" class="form-control validate">
                            <label data-error="wrong" data-success="right" for="form32">Subject</label>
                          </div>

                          <div class="md-form">
                            <i class="fas fa-pencil prefix grey-text"></i>
                            <textarea type="text" id="form8" class="md-textarea form-control" rows="4"></textarea>
                            <label data-error="wrong" data-success="right" for="form8">Your message</label>
                          </div>

                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                          <button class="btn blue-gradient">Send <span class="glyphicon glyphicon-send"></span></button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="text-center pa">
                    <a class="btn blue-gradient btn-rounded mb-4" data-toggle="modal" data-target="#modalContactForm">
                      Contact us
                    </a>
                  </div>
                </div>
                <div class="col-lg-7 col-md-12 hidden-xs">
                  <img class="img-fluid rounded mx-auto d-block" src="<?php base_url() ?>plantilla/imagenes/contact us.png" width="600" alt="">
                </div>
              </div>
            </article>
          </div>

        </section>
      </div>
    </section>

    <footer class="page-footer font-small fondon pt-4">
      <div class="container-fluid text-center text-md-left">
        <div class="row secction">

          <div class="col-md-6 mt-md-0 mt-3">
                  <h5 class="text-uppercase co12">PRAGRAM ME PROJECTS</h5>
                  <p class="co1 hidden-xs">Here you can use rows and columns here to organize your footer content.</p>
                </div>
          <hr class="clearfix w-100 d-md-none pb-3 bord1">
          <div class="col-md-3 mb-md-0 mb-3 co1">

                    <h5 class="text-uppercase co12">social networks</h5>

                    <ul class="list-unstyled co1 pa12">
                      <li>
                        <a href="#">
                          <i class="fab fa-facebook-f blue-text"> </i> Faceboock
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fab fa-instagram blue-text"> </i> Instagran
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fab fa-twitter blue-text"> </i> Twitter
                        </a>
                      </li>
                    </ul>

                </div>
          <div class="col-md-3 mb-md-0 mb-3">

                    <h5 class="text-uppercase co12">Data</h5>

                    <ul class="list-unstyled co1 pa12">
                      <li>
                        <a href="#">
                          <span class="glyphicon glyphicon-home"></span> Our offices
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <span class="glyphicon glyphicon-book"></span> Contact information
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <span class="glyphicon glyphicon-envelope"></span> E-mail
                        </a>
                      </li>
                    </ul>

                </div>

        </div>
      </div>
      <div class="footer-copyright text-center py-3 co1">© 2019 Copyright:
        <a href="#"> PMP.com</a>
      </div>
    </footer>
